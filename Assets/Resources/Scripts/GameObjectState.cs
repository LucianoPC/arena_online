﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameObjectState<T> {

	public const int DEFAULT_PRIORITY = 0;

	public delegate void GameObjectStateListener ();
	public GameObjectStateListener onExit;


	public T Main { get { return this.main; } }
	public int Priority { get { return this.priority; } }
	public bool IsCurrentState { get { return this.isCurrentState; } }

	private T main;
	private int priority;
	private bool isCurrentState;

	public void Awake (T main, int priority = DEFAULT_PRIORITY) {
		this.main = main;
		this.priority = priority;
		this.isCurrentState = false;

		OnAwake ();
	}

	public void Enter () {
		this.isCurrentState = true;
		OnEnter ();
	}

	public void Exit () {
		this.isCurrentState = false;
		OnExit ();

		if (this.onExit != null) this.onExit ();
	}


	public virtual void FixedUpdate () {}
	public virtual void Update () {}
	public virtual void LateUpdate () {}

	public virtual bool CanEnter () { return true; }

	protected virtual void OnAwake () {}
	protected virtual void OnEnter () {}
	protected virtual void OnExit () {}

}
