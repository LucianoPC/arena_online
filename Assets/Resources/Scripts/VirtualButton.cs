﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	private bool buttonHeldDown;
	private bool buttonPressedDown;
	private bool buttonPressedUp;
	

	void Awake () {
		this.buttonHeldDown = false;
		this.buttonPressedUp = false;
		this.buttonPressedDown = false;
	}

	void FixedUpdate () {
		this.buttonPressedUp = false;
		this.buttonPressedDown = false;
	}

	public virtual void OnPointerDown (PointerEventData pointerEventData) {
		this.buttonHeldDown = true;
		this.buttonPressedDown = true;
	}

	public virtual void OnPointerUp (PointerEventData pointerEventData) {
		this.buttonHeldDown = false;
		this.buttonPressedUp = true;
	}

	public bool Button () {
		return this.buttonHeldDown;
	}

	public bool ButtonUp () {
		return this.buttonPressedUp;
	}

	public bool ButtonDown () {
		return this.buttonPressedDown;
	}
}
