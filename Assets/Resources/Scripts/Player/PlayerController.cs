﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Vector3 axis;

	private Player player;
	private VirtualStick stick;
	private VirtualButton buttonDash;
	private VirtualButton buttonShot;

	void Awake () {
		this.player = GetComponent<Player> ();
		this.stick = GameObject.Find ("Canvas/VirtualJoystick/Stick").GetComponent<VirtualStick> ();
		this.buttonDash = GameObject.Find ("Canvas/VirtualJoystick/ButtonDash").GetComponent<VirtualButton> ();
		this.buttonShot = GameObject.Find ("Canvas/VirtualJoystick/ButtonShot").GetComponent<VirtualButton> ();
	}

	void LateUpdate () {
		Move ();
		Dash ();
		Shot ();
	}

	private void Move () {
		this.axis.x = this.stick.Horizontal ();
		this.axis.z = this.stick.Vertical ();

		this.player.Axis = this.axis;
	}

	private void Dash () {
		if (this.buttonDash.ButtonDown ())
			this.player.Trigger.Call ("dash");
	}

	private void Shot () {
		if (this.buttonShot.ButtonDown ())
			this.player.Trigger.Call ("shot_begin");
		
		else if (this.buttonShot.ButtonUp ())
			this.player.Trigger.Call ("shot_fire");
	}
}
