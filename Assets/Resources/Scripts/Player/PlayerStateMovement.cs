﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerStateMovement : GameObjectState<Player> {

	public float speedScale = 1f;

	private Vector3 velocity;
	private ParticleSystem movementParticle;


	protected override void OnAwake () {
		this.movementParticle = this.Main.transform.Find ("MovementParticle").GetComponent<ParticleSystem> ();
	}

	public override void FixedUpdate () {
		Move ();
	}

	protected override void OnEnter () {
		this.movementParticle.Play ();
	}

	protected override void OnExit () {
		this.movementParticle.Stop ();
	}

	private void Move () {
		this.velocity = this.Main.Axis * this.Main.speed * this.speedScale;
		this.velocity.y = this.Main.Rigidbody.velocity.y;

		this.Main.Rigidbody.velocity = this.velocity;

		if(this.velocity.sqrMagnitude > 0.001f)
			this.Main.transform.forward = this.velocity;
	}
}
