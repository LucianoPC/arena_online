﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerStateDash : GameObjectState<Player> {

	public float speedScale = 4f;
	public float dashTime = 0.25f;
	public float cooldownTime = 1.5f;

	private float finishDashTime;
	private float finishCooldownTime;

	private Vector3 velocity;
	private Vector3 direction;

	private ParticleSystem dashParticle;


	protected override void OnAwake () {
		this.finishDashTime = 0f;
		this.finishCooldownTime = 0f;

		this.dashParticle = this.Main.transform.Find ("DashParticle").GetComponent<ParticleSystem> ();
	}

	public override void FixedUpdate () {
		CheckFinished ();
		Move ();
	}

	public override bool CanEnter () { 
		return Time.unscaledTime >= this.finishCooldownTime; 
	}

	protected override void OnEnter () {
		this.dashParticle.Play ();
		this.direction = this.Main.transform.forward;
		this.finishDashTime = Time.unscaledTime + this.dashTime;
		this.Main.Animator.SetBool (Player.AnimationHash.DASH, true);
	}

	protected override void OnExit () {
		this.dashParticle.Stop ();
		this.finishCooldownTime = Time.unscaledTime + this.cooldownTime;
		this.Main.Animator.SetBool (Player.AnimationHash.DASH, false);
	}

	private void CheckFinished () {
		if (Time.unscaledTime < this.finishDashTime) return;
		Exit ();
	}

	private void Move () {
		if (!this.IsCurrentState) return;

		this.velocity = this.direction * this.Main.speed * this.speedScale;
		this.velocity.y = this.Main.Rigidbody.velocity.y;

		this.Main.Rigidbody.velocity = this.velocity;

		if(this.velocity.sqrMagnitude > 0.001f)
			this.Main.transform.forward = this.velocity;
	}
}
