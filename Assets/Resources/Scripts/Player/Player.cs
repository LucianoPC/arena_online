﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[System.Serializable] public class PlayerStateMachine : GameObjectStateMachine<Player> {}

	public static class AnimationHash {
		public static int DASH = Animator.StringToHash ("dash");
		public static int SHOT = Animator.StringToHash ("shot");
		public static int VELOCITY = Animator.StringToHash ("velocity");
	}


	public float speed = 2f;

	public Vector3 Axis { get { return this.axis; } set { SetAxis (value); } }
	public Trigger Trigger { get { return this.trigger; } }
	public Animator Animator { get { return this.animator; } }
	public Rigidbody Rigidbody { get { return this.rigidbody; } }


	private Trigger trigger;
	private Animator animator;
	private new Rigidbody rigidbody;
	[SerializeField] private Vector3 axis;
	[SerializeField] private PlayerStateDash dashState;
	[SerializeField] private PlayerStateShot shotState;
	[SerializeField] private PlayerStateMovement movementState;
	[SerializeField] private PlayerStateMachine stateMachine;


	void Awake () {
		this.rigidbody = GetComponent<Rigidbody> ();

		this.animator = transform.Find ("Model").GetComponent<Animator> ();

		this.dashState.Awake (this, 1);
		this.shotState.Awake (this, 2);
		this.movementState.Awake (this, 0);
		this.stateMachine.Awake (this.movementState);

		this.trigger = new Trigger ();
		this.trigger.Add ("dash", () => this.stateMachine.SetState (this.dashState));
		this.trigger.Add ("shot_begin", () => this.stateMachine.SetState (this.shotState));
		this.trigger.Add ("shot_fire", () => this.shotState.Fire ());
	}

	void FixedUpdate () {
		this.trigger.CheckTriggers ();
		this.stateMachine.FixedUpdate ();
	}

	void Update () {
		this.stateMachine.Update ();
	}

	void LateUpdate () {
		this.stateMachine.LateUpdate ();
		SetAnimationVelocity ();
	}

	void OnValidate () {
		this.Axis = this.axis;
	}

	private void SetAxis (Vector3 axis) {
		axis.y = 0;
		this.axis = axis.sqrMagnitude > 1f ? axis.normalized : axis;
	}

	private void SetAnimationVelocity () {
		float velocity = (this.rigidbody.velocity / this.speed).magnitude;
		this.animator.SetFloat (AnimationHash.VELOCITY, velocity);
	}
}
