﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerStateShot : GameObjectState<Player> {

	public float shotSpeed = 7.5f;
	public float cooldownTime = 1.5f;
	public GameObject bulletPrefab;

	private bool fire;
	private float finishCooldownTime;
	private Transform bulletSpawn;
	private GameObject bulletObject;


	protected override void OnAwake () {
		this.finishCooldownTime = 0f;
		this.bulletSpawn = this.Main.transform.Find ("BulletSpawn");
	}

	public override void FixedUpdate () {
		SetDirection ();
	}

	public override bool CanEnter () { 
		return Time.unscaledTime >= this.finishCooldownTime; 
	}

	protected override void OnEnter () {
		this.fire = false;

		this.bulletObject = MonoBehaviour.Instantiate (this.bulletPrefab, this.bulletSpawn);

		this.Main.Rigidbody.velocity = Vector3.zero;
		this.Main.Animator.SetBool (Player.AnimationHash.SHOT, true);
	}

	protected override void OnExit () {
		this.finishCooldownTime = Time.unscaledTime + this.cooldownTime;
		this.Main.Animator.SetBool (Player.AnimationHash.SHOT, false);
	}

	public void Fire () {
		if (!this.IsCurrentState) return;

		this.fire = true;

		this.bulletObject.GetComponent<Bullet> ().Fire (bulletSpawn.transform.forward * this.shotSpeed);

		MonoBehaviour.Destroy (this.bulletObject, 2.0f);

		Exit ();
	}

	private void SetDirection () {
		if(this.Main.Axis.sqrMagnitude > 0.001f)
			this.Main.transform.forward = this.Main.Axis;
	}
}
