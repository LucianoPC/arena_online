﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectStateMachine<T> {

	public GameObjectState<T> CurrentState { get { return this.currentState; } }

	private GameObjectState<T> currentState;
	private GameObjectState<T> defaultState;
	private Queue<GameObjectState<T>> stateQueue;


	public void Awake (GameObjectState<T> defaultState) {
		this.defaultState = defaultState;
		this.stateQueue = new Queue<GameObjectState<T>> ();

		SetState (this.defaultState);
	}

	public void FixedUpdate () {
		this.CurrentState.FixedUpdate ();
	}

	public void Update () {
		this.CurrentState.Update ();
	}

	public void LateUpdate () {
		this.CurrentState.LateUpdate ();
	}

	public void SetState (params GameObjectState<T>[] states) {
		if (!states [0].CanEnter ()) return;
		if (this.currentState != null && states [0].Priority <= this.currentState.Priority) return;

		this.stateQueue.Clear ();

		for (int index = 0; index < states.Length; index++) {
			states[index].onExit = NextState;

			this.stateQueue.Enqueue (states[index]);
		}

		if (this.currentState == null) NextState ();
		else this.currentState.Exit ();
	}

	private void NextState () {
		if (this.stateQueue.Count == 0) this.currentState = this.defaultState;
		else this.currentState = this.stateQueue.Dequeue ();

		this.currentState.Enter ();
	}
}
