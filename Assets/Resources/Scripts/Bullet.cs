﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {


	private new Rigidbody rigidbody;
	private ParticleSystem particle;


	void Awake () {
		this.rigidbody = GetComponent<Rigidbody> ();
		this.particle = transform.Find ("Particle").GetComponent<ParticleSystem> ();
	}

	public void Fire (Vector3 velocity) {
		this.transform.parent = null;
		this.rigidbody.velocity = velocity;

		this.particle.Play ();
	}
}
