﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger {

	private List<string> triggerNames;
	private Dictionary<string, bool> boolMap;
	private Dictionary<string, Action> actionMap;

	public Trigger () {
		this.triggerNames = new List<string> ();

		this.boolMap = new Dictionary<string, bool>();
		this.actionMap = new Dictionary<string, Action>();
	}

	public void Add (string triggerName, Action action) {
		if (this.triggerNames.Contains (triggerName)) return;

		this.triggerNames.Add (triggerName);

		this.boolMap.Add (triggerName, false);
		this.actionMap.Add (triggerName, action);
	}

	public void Call (string triggerName) {
		if (!this.triggerNames.Contains (triggerName)) return;

		this.boolMap [triggerName] = true;
	}

	public void CheckTriggers () {
		string triggerName;

		for (int index = 0; index < this.triggerNames.Count; index++) {
			triggerName = this.triggerNames [index];

			if (!this.boolMap [triggerName]) continue;

			this.boolMap [triggerName] = false;
			this.actionMap [triggerName] ();
		}
	}
}
