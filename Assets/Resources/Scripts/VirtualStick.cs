﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualStick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
	
	private Image uiStickImage;
	private Image uiStickBackgroundImage;

	private Vector3 inputVector;
	private Vector2 outPosition;
	private Vector3 stickPosition;


	void Awake () {
		this.uiStickImage = transform.Find ("StickImage").GetComponent<Image> ();
		this.uiStickBackgroundImage = transform.GetComponent<Image> ();
	}

	public virtual void OnDrag (PointerEventData pointerEventData) {
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (this.uiStickBackgroundImage.rectTransform,
			pointerEventData.position, pointerEventData.pressEventCamera, out this.outPosition)) {

			this.inputVector.x = this.outPosition.x * 2f / this.uiStickBackgroundImage.rectTransform.sizeDelta.x;
			this.inputVector.y = 0f;
			this.inputVector.z = this.outPosition.y * 2f / this.uiStickBackgroundImage.rectTransform.sizeDelta.y;

			if (this.inputVector.sqrMagnitude > 1f) this.inputVector.Normalize ();

			this.stickPosition.x = this.inputVector.x * this.uiStickBackgroundImage.rectTransform.sizeDelta.x / 2.75f;
			this.stickPosition.y = this.inputVector.z * this.uiStickBackgroundImage.rectTransform.sizeDelta.y / 2.75f;
			this.stickPosition.z = 0f;

			this.uiStickImage.rectTransform.anchoredPosition = this.stickPosition;
		}
	}

	public virtual void OnPointerDown (PointerEventData pointerEventData) {
		OnDrag (pointerEventData);
	}

	public virtual void OnPointerUp (PointerEventData pointerEventData) {
		this.inputVector = Vector3.zero;
		this.uiStickImage.rectTransform.anchoredPosition = this.inputVector;
	}

	public float Horizontal () {
		if (this.inputVector.x != 0f)
			return this.inputVector.x;
		else
			return Input.GetAxis ("Horizontal");
	}

	public float Vertical () {
		if (this.inputVector.z != 0f)
			return this.inputVector.z;
		else
			return Input.GetAxis ("Vertical");
	}
}
